package examenisp2020;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.*;
import java.util.*;

public class s2 extends JFrame {
	JLabel info1;
	JTextField text1,text2;
	JButton buton1;
	
	s2(){
		 
        setTitle("Subiectul 2");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(200,250);
        setVisible(true);
  }
	
	public void init(){
		 
        this.setLayout(null);
        int width=100;int height = 40;

        info1 = new JLabel("Fisier:");
        info1.setBounds(10, 50, width, height);
        
        text1 = new JTextField();
        text1.setBounds(80,50,width, height);

        text2 = new JTextField();
        text2.setBounds(80,100,width, height);

        buton1 = new JButton("Calculeaza");
        buton1.setBounds(30,150,width, height);

        buton1.addActionListener(new calculeaza());


        add(info1);add(text1);add(text2);add(buton1);
        }
	
	public static void main(String[] args) {
        new s2();
    }
	
	class calculeaza implements ActionListener{
	  	  
        public void actionPerformed(ActionEvent e)  {

        	int caractere = 0;
         	char x;
        	String date = null;
      	    String fisier = s2.this.text1.getText();
            
      	    File read = new File("W:\\FACULTATE\\ANUL 2\\SEMESTRUL 2\\ISP\\ISP\\workspace\\examenisp2020\\src\\examenisp2020\\" + fisier);
         	  try (Scanner citire = new Scanner(read)) {
         		   while(citire.hasNextLine())
         		      { date = citire.nextLine();
         	         	for(int i = 0; i < date.length(); i++)
         	         	{
         	         		x = date.charAt(i);
         	         		if(x != ' ') caractere++;
         	         	}
         	           }
                   } 
         	       catch (FileNotFoundException e1) {
					e1.printStackTrace();
				   } 
         	  s2.this.text2.setText("Caractere:" + caractere);
      }
  
 }

	
}
